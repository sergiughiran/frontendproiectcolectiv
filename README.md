# iOS Project Skeleton

This is a project template with commonly used solutions, architecture and frameworks. Useful when starting a new project.

## General

* iOS Version: 11+
* Swift Version: 4.2 
* Dependency Management: [CocoaPods](https://cocoapods.org)

## Prerequirements

* [bundler](https://bundler.io)
* [homebrew](https://brew.sh)

## How to use this repository

1. Clone this repository locally via command line or SourceTree.

```bash
git clone git@bitbucket.org:halcyonmobile/ios-project-skeleton.git
```

2. Install xcodegen

Xcodegen is used to generate a project.

```
brew install xcodegen
```

3. Run the generate script

The script will take you all the steps required to generate the project.

```bash
sh generate.sh
```

## What's included

### Architecture

* Directory structure
* Flow controllers

### Frameworks

* [RestBird](https://github.com/halcyonmobile/RestBird) - In-house REST networking client
* [SwiftGen](https://github.com/SwiftGen/SwiftGen) - Generate precompiled files for assets and resources.
* [SwiftLint](https://github.com/realm/SwiftLint) - Swift linter to enforce code style rules

## Get started with CI / CD

- TBD
