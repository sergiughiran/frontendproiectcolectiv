default_platform(:ios)

platform :ios do
  
  scheme = "{app_name}"
  sources_path = "Source"

  desc "Install dependencies"
  lane :dependencies do
    cocoapods
  end
  
  desc "Runs all the tests"
  lane :test do
    scan
  end

  desc "Collect metrics information about the project"
  desc "Also uploads it to SonarQube"
  lane :metrics do
    slather(cobertura_xml: true, jenkins: true, scheme: scheme, build_directory: "./derivedData", output_directory: "./reports", proj: "./#{scheme}.xcodeproj")
    sh("cd .. && lizard './#{sources_path}' -l swift --xml > ./reports/lizard-report.xml")
    swiftlint(executable: "Pods/SwiftLint/swiftlint", output_file: "./reports/swiftlint.txt", ignore_exit_status: true)
    sonar(
      sonar_login: "492b06d9691ef6abba12def70168c420038b3878",
      project_key: "com.halcyonmobile.asd",
      project_version: version_number,
      project_name: scheme,
      sources_path: File.expand_path("../#{sources_path}")
    )
  end

  desc "Staging build distribution"
  desc "options"
  desc "  - nightly: bool"
  lane :distribute_staging do |options|
    sync_code_signing(readonly: true)

    increment_build_number(build_number: ENV["BUILD_NUMBER"])

    if options[:nightly]
      # add badge
    end

    build_app(
      workspace: "#{scheme}.xcworkspace",
      scheme: scheme,
      configuration: "Debug",
      silent: true,
      clean: true,
      export_method: "development"
    )

    crashlytics(groups: ['internal-testers'], notes: "v#{version_number} [Nightly]")
  end 

  desc "Production build distribution"
  lane :distribute_production do |options|
    sync_code_signing(type: "appstore", readonly: true)

    build_app(
      workspace: "#{scheme}.xcworkspace",
      scheme: scheme,
      configuration: "Release",
      silent: true,
      clean: true,
      export_method: "appstore"
    )

    # App store upload.
    upload_to_app_store
  end
end
