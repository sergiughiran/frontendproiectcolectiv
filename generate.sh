#!/bin/sh

APP_NAME="MyProject"

if ! command -v xcodegen 2>/dev/null; then
    echo "You need xcodegen to generate the project."
    echo "Install by running: brew install xcodegen"
    exit 1
fi

# Generate project 

xcodegen

sed -i '' "s/{app_name}/$APP_NAME/g" Podfile

# Install dependencies

bundle install

bundle exec pod install

# clearning up

echo "Cleaning up ..."