//
//  Empty.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import Foundation

struct Empty: Codable {
    var message: String
}
