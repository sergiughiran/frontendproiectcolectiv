//
//  User.swift
//  project-skeleton
//
//  Created by Botond Magyarosi on 12/03/2017.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import Foundation

struct User: Codable {
    var _id: String
    var email: String
    var displayName: String
    var skills: String?
    var isClient: Bool
}
