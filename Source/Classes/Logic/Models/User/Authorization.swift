//
//  Authorization.swift
//  Acceptance Tests
//
//  Created by Botond Magyarosi on 08/01/2019.
//

import Foundation

struct Authorization: Decodable {
    let token: String
}
