//
//  Job.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

struct Job: Codable {
    var _id: String
    var title: String
    var description: String
    var _creator: String
    var applicants: [String]
    var assignee: String?
}

struct NoReply: Codable {}
