//
//  AuthenticationService.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation
import RxSwift

final class AuthenticationService {
    
    var isAuthenticated: BehaviorSubject<Bool> = BehaviorSubject<Bool>(value: false)
    
    init() {
        if KeychainManager.shared.token != nil {
            isAuthenticated.onNext(true)
        }
    }
    
    func logIn(email: String, password: String, success: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: LogInRequest(email: email, password: password), success: success, failure: failure)
    }
    
    func register(email: String, password: String, isClient: Bool, skills: String?, displayName: String, success: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: SignUpRequest(email: email, password: password, isClient: isClient, skills: skills, photoURL: nil, displayName: displayName), success: success, failure: failure)
    }
    
    func logOut(success: @escaping (Empty) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: LogOutRequest(), success: success, failure: failure)
    }
}
