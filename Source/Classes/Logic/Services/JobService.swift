//
//  JobService.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

final class JobService {
    func getJobList(success: @escaping ([Job]) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: JobListRequest(), success: success, failure: failure)
    }
    
    func getMyJobs(success: @escaping ([Job]) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: MyJobRequest(), success: success, failure: failure)
    }
    
    func getUser(id: String, success: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: UserRequest(id: id), success: success, failure: failure)
    }
    
    func getApplicants(id: String, success: @escaping ([User]) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: ApplicantsRequest(id: id), success: success, failure: failure)
    }
    
    func assignJob(jobID: String, userID: String, success: @escaping (Job) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: AssignRequest(jobID: jobID, userID: userID), success: success, failure: failure)
    }
    
    func apply(jobID: String, success: @escaping (Job) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: ApplyRequest(jobID: jobID), success: success, failure: failure)
    }
    
    func addJob(title: String, description: String, success: @escaping (Job) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: AddJobRequest(title: title, description: description), success: success, failure: failure)
    }
    
    func updateClient(email: String, username: String, isClient: Bool, success: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: UpdateProfileRequest(email: email, displayName: username, isClient: isClient, skills: nil), success: success, failure: failure)
    }
    
    func updateProvider(email: String, username: String, skills: String?, isClient: Bool, success: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: UpdateProfileRequest(email: email, displayName: username, isClient: isClient, skills: skills), success: success, failure: failure)
    }
    
    func updatePassword(oldPassword: String, newPassword: String, success: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: UpdatePasswordRequest(oldPassword: oldPassword, newPassword: newPassword), success: success, failure: failure)
    }
    
    func getUsers(success: @escaping ([User]) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: UserListRequest(), success: success, failure: failure)
    }
    
    func requestUser(userID: String, jobID: String, success: @escaping (User) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: RequestUserRequest(userID: userID, jobID: jobID), success: success, failure: failure)
    }
    
    func getUserRequests(success: @escaping ([Job]) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: UserRequestsRequest(), success: success, failure: failure)
    }
    
    func acceptRequest(userID: String, jobID: String, success: @escaping (Job) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: AcceptRequestRequest(userID: userID, jobID: jobID), success: success, failure: failure)
    }
    
    func subscribeToNewsletter(success: @escaping (NoReply) -> Void, failure: @escaping (Error) -> Void) {
        APIClient.shared.execute(request: NewsletterRequest(), success: success, failure: failure)
    }
}
