//
//  AppEngine.swift
//  project-skeleton
//
//  Created by Tamas Levente on 9/22/17.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import Foundation

class AppEngine {
    
    // MARK: - Lifecycle
    
    init() {
        // TODO: Setup logging and Fabric
//        Logger.setupLogging()
//        Fabric.with([Crashlytics.self])
    }
    
    // MARK: - Application events
    
    func prepareAppStart() {
        startDebugToolsIfNeeded()
    }
}

extension AppEngine {
    
    fileprivate func startDebugToolsIfNeeded() {
        // TODO: Start debugging tools (netfox for example)
//        if AppLaunchConfig.useNetfox {
//            NFX.sharedInstance().start()
//        }
    }
}
