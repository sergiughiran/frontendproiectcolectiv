//
//  KeychainStore.swift
//  Acceptance Tests
//
//  Created by Botond Magyarosi on 08/01/2019.
//

import Foundation

// MARK: - Lightweight keychain wrapper

// TODO: Think about kSecAttrAccessible

internal class KeychainStore {

    typealias Key = String

    private static let defaultServiceName: String = {
        return Bundle.main.bundleIdentifier!
    }()

    /// Identify keychain items by the service name
    private let serviceName: String

    // MARK: - Lifecycle

    init(serviceName: String?) {
        self.serviceName = serviceName ?? KeychainStore.defaultServiceName
    }

    // MARK: - Set

    @discardableResult
    func set(
        _ value: Data?,
        forKey key: Key
    ) -> Bool {
        guard let value = value else {
            return delete(forKey: key)
        }
        return setData(value, forKey: key)
    }

    private func setData(
        _ value: Data,
        forKey key: Key
    ) -> Bool {
        var attributes = makeAttributes(forKey: key)
        attributes[kSecValueData as String] = value

        let status = SecItemAdd(attributes as CFDictionary, nil)

        switch status {
        case errSecSuccess:
            return true
        case errSecDuplicateItem:
            return update(value, forKey: key)
        default:
            return false
        }
    }

    // MARK: - Update

    private func update(
        _ value: Data,
        forKey key: Key
    ) -> Bool {
        let attributes = makeAttributes(forKey: key)
        let updateAttributes: [String: Any] = [(kSecValueData as String): value]

        let status: OSStatus = SecItemUpdate(attributes as CFDictionary, updateAttributes as CFDictionary)
        return status == noErr
    }

    // MARK: - Get

    private func getData(
        forKey key: Key
    ) -> Data? {
        var attributes = makeAttributes(forKey: key)
        attributes[kSecMatchLimit as String] = kSecMatchLimitOne
        attributes[kSecReturnData as String] = kCFBooleanTrue

        var result: AnyObject?
        let status = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(attributes as CFDictionary, UnsafeMutablePointer($0))
        }
        return status == noErr ? result as? Data : nil
    }

    // MARK: - Delete

    @discardableResult
    private func delete(
        forKey key: Key
    ) -> Bool {
        let attributes = makeAttributes(forKey: key)
        let status = SecItemDelete(attributes as CFDictionary)
        return status == noErr
    }

    // MARK: - Utilities

    private func makeAttributes(forKey key: Key) -> [String: Any] {
        return [
            (kSecClass as String): kSecClassGenericPassword,
            (kSecAttrGeneric as String): key.data(using: .utf8)!,
            (kSecAttrService as String): serviceName] as [String: Any]
    }
}

// MARK: - String

extension KeychainStore  {

    func get(
        forKey key: Key
    ) -> String? {
        guard let data = getData(forKey: key) else { return nil }
        return String(data: data, encoding: .utf8)
    }

    @discardableResult
    func set(
        _ value: String?,
        forKey key: Key
    ) -> Bool {
        return set(value?.data(using: .utf8), forKey: key)
    }
}
