//
//  KeychainManager.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation
import SwiftKeychainWrapper

class KeychainManager {
    
    static let shared = KeychainManager()
    private let keychain = KeychainWrapper.standard
    
    var token: String? {
        willSet {
            setToken(to: newValue)
        }
    }
    
    init() {
//        keychain.removeObject(forKey: "token")
        token = keychain.string(forKey: "token")
    }
    
    private func setToken(to newValue: String?) {
        guard let token = newValue else {
            keychain.removeObject(forKey: "token")
            return
        }
        keychain.set(token, forKey: "token")
    }
}

