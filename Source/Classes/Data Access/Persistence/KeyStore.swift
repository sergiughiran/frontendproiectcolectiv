//
//  KeyStore.swift
//  Acceptance Tests
//
//  Created by Botond Magyarosi on 08/01/2019.
//

import Foundation

private enum Key {
    static let accessToken          = "accessToken"
}

class KeyStore {

    private let userDefaults = UserDefaults.standard
    private let keychainStore = KeychainStore(serviceName: "com.application.identifier")

    // MARK: - Lifecycle

    static let shared = KeyStore()

    private init() {}

    // MARK: - Methods

    var accessToken: String? {
        get { return keychainStore.get(forKey: Key.accessToken) }
        set { keychainStore.set(accessToken, forKey: Key.accessToken) }
    }
}

