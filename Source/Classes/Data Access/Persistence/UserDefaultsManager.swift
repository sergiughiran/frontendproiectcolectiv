//
//  UserDefaultsManager.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

class UserDefaultsManager {
    static let shared = UserDefaultsManager()
    
    var manager = UserDefaults.standard
    
    var user: User? {
        get {
            guard let userData = manager.data(forKey: "user") else { return nil }
            
            do {
                let decodedUser = try? JSONDecoder().decode(User.self, from: userData)
                return decodedUser
            } catch {
                print("ERROR")
                return nil
            }
        }
        set {
            guard let user = newValue else {
                manager.removeObject(forKey: "user")
                return
            }
            
            do {
                let encodedUser: Data = try JSONEncoder().encode(user)
                manager.set(encodedUser, forKey: "user")
            } catch {
                print("ERROR")
            }
        }
    }
}
