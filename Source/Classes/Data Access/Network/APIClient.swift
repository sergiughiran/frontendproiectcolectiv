//
//  APIClient.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation
import Alamofire

struct EmptyResponse: Codable { }

class APIClient {
    
    static let shared = APIClient()
    
    private var header: [String: String]? {
        guard let token = KeychainManager.shared.token else { return nil }
        return ["x-auth": token]
    }
    
    func execute<T: Decodable>(request: APIRequest, success: @escaping (T) -> Void, failure: @escaping (Error) -> Void) {
        let req = asURLRequest(request: request)
        req.validate().responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                
                if let token = response.response?.allHeaderFields["x-auth"] as? String {
                    KeychainManager.shared.token = token
                }
                
                do {
                    let returnValue =  try JSONDecoder().decode(T.self, from: data)
                    success(returnValue)
                } catch let error {
                    failure(error)
                }
            case .failure(let error):
                guard let statusCode = response.response?.statusCode else { return }
                failure(error)
            }
        })
    }
    
    func execute<T: Decodable>(request: APIRequest, success: @escaping ([T]) -> Void, failure: @escaping (Error) -> Void) {
        let req = asURLRequest(request: request)
        req.validate().responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                guard let data = response.data else { return }
                do {
                    let returnValue =  try JSONDecoder().decode([T].self, from: data)
                    success(returnValue)
                } catch let error {
                    failure(error)
                }
            case .failure(let error):
                guard let statusCode = response.response?.statusCode else { return }
                failure(error)
            }
        })
    }
    
    // MARK: - Private
    
    private func asURLRequest(request: APIRequest) -> Alamofire.DataRequest {
        let suffix = request.path
        
        let encoding: ParameterEncoding
        if request.method == .get {
            encoding = URLEncoding.default
        } else {
            encoding = JSONEncoding.default
        }
        return Alamofire.request("https://conectum.tk/api" + suffix,
                                 method: request.method,
                                 parameters: request.parameters,
                                 encoding: encoding,
                                 headers: header)
    }
}
