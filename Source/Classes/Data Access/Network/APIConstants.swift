//
//  APIConstants.swift
//  Acceptance Tests
//
//  Created by Botond Magyarosi on 08/01/2019.
//

import Foundation

enum API {

#if DEBUG
    static let backendURL = "https://foo.bar/api"
#else
    static let backendURL = "https://foo.bar/api"
#endif

    enum Path {
        static let login        = "/auth/login"
        static let signup       = "/auth/signup"
    }

    enum Param {
        static let id           = "id"
        static let email        = "email"
        static let password     = "password"
    }
}
