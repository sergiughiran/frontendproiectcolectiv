//
//  AuthRequests.swift
//  Acceptance Tests
//
//  Created by Botond Magyarosi on 08/01/2019.
//

import Foundation
import RestBird

// MARK: - Log in

extension Request.Auth {

    struct Login: DataRequest {
        typealias ResponseType = Authorization

        let email: String
        let password: String

        let suffix: String? = API.Path.login
        let method: HTTPMethod = .post

        var parameters: RequestParameters? {
            return [
                API.Param.email: email,
                API.Param.password: password
            ]
        }
    }
}

// MARK: - Sign up

extension Request.Auth {

//    If there's no parameter change
//    The following can be used
//
//    typealias SignUp = Login

    struct SignUp: DataRequest {
        typealias ResponseType = Authorization

        let email: String
        let password: String

        let suffix: String? = API.Path.signup
        let method: HTTPMethod = .post

        var parameters: RequestParameters? {
            return [
                API.Param.email: email,
                API.Param.password: password
            ]
        }
    }
}

