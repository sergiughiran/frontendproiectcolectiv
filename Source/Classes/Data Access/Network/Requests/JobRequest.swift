//
//  JobRequest.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation
import Alamofire

struct JobListRequest: APIRequest {
    var method: HTTPMethod = .get
    var path: String = "/jobs"
}

struct MyJobRequest: APIRequest {
    var method: HTTPMethod = .get
    var path: String = "/myjobs"
}

struct UserRequest: APIRequest {
    var method: HTTPMethod = .get
    var path: String = "/user/"
    
    init(id: String) {
        path += id
    }
}

struct ApplicantsRequest: APIRequest {
    var method: HTTPMethod = .get
    var path: String = "/getApplicants/"
    
    init(id: String) {
        path += id
    }
}

struct AssignRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/assign"
    var parameters: Parameters? = [:]
    
    init(jobID: String, userID: String) {
        parameters = [
            "jobID": jobID,
            "userID": userID
        ]
    }
}

struct ApplyRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/apply"
    var parameters: Parameters? = [:]
    
    init(jobID: String) {
        parameters = ["jobID": jobID]
    }
}

struct AddJobRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/jobs"
    var parameters: Parameters? = [:]
    
    init(title: String, description: String) {
        parameters = [
            "title": title,
            "description": description
        ]
    }
}

struct UpdateProfileRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/updateUser"
    var parameters: Parameters? = [:]
    
    init(email: String, displayName: String, isClient: Bool, skills: String?) {
        parameters = [
            "email": email,
            "displayName": displayName,
            "isClient": isClient,
            "skills": skills
        ]
    }
}

struct UpdatePasswordRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/changePass"
    var parameters: Parameters? = [:]
    
    init(oldPassword: String, newPassword: String) {
        parameters = [
            "oldPassword": oldPassword,
            "newPassword": newPassword
        ]
    }
}

struct UserListRequest: APIRequest {
    var method: HTTPMethod = .get
    var path: String = "/users"
}

struct RequestUserRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/request/"
    var parameters: Parameters? = [:]
    
    init(userID: String, jobID: String) {
        path += userID
        parameters = ["jobID" : jobID]
    }
}

struct UserRequestsRequest: APIRequest {
    var method: HTTPMethod = .get
    var path: String = "/requested"
}

struct AcceptRequestRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/assign"
    var parameters: Parameters? = [:]
    
    init(userID: String, jobID: String) {
        parameters = ["userID": userID, "jobID": jobID]
    }
}

struct NewsletterRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/subscribeViaToken"
}
