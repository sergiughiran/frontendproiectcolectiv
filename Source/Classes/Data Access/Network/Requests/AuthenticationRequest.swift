//
//  AuthenticationRequest.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation
import Alamofire

struct LogInRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/login"
    var parameters: Parameters? = [:]
    
    init(email: String, password: String) {
        parameters = ["email": email, "password": password]
    }
}

struct SignUpRequest: APIRequest {
    var method: HTTPMethod = .post
    var path: String = "/signup"
    var parameters: Parameters? = [:]
    
    init(email: String, password: String, isClient: Bool, skills: String?, photoURL: String?, displayName: String) {
        parameters = [
            "email": email,
            "password": password,
            "isClient": isClient,
            "skills": skills,
            "photoURL": photoURL,
            "displayName": displayName
        ]
    }
}

struct LogOutRequest: APIRequest {
    var method: HTTPMethod = .delete
    var path: String = "/logout"
}
