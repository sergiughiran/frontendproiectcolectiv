//
//  APIRequest.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation
import Alamofire

protocol APIRequest {
    
    // TODO: Expected type typealias for all requests.
    
    
    /// HTTP method for the request
    var method: HTTPMethod { get }
    
    /// API path
    var path: String { get }
    
    /// Request parameters.
    /// Default is empty
    var parameters: Parameters? { get }
}

// MARK: - Default parameters

extension APIRequest {
    
    var parameters: Parameters? { return nil }
}
