//
//  APIConfiguration.swift
//  Acceptance Tests
//
//  Created by Botond Magyarosi on 08/01/2019.
//

import Foundation
import RestBird
import Alamofire

struct APIConfiguration: NetworkClientConfiguration {

    let baseUrl: String
    let sessionManager: RestBird.SessionManager
    let jsonDecoder: JSONDecoder
    let jsonEncoder: JSONEncoder

    init() {
        // URL
        baseUrl = API.backendURL

        // URL Session
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        // TODO: Add Authnotirization token
//        if let accessToken = LocalStorageManager.shared.accessToken {
//            configuration.httpAdditionalHeaders?["Authorization"] = accessToken
//        }
        sessionManager = AlamofireSessionManager(sessionManager: Alamofire.SessionManager(configuration: configuration))

        // Serialization
        jsonEncoder = JSONEncoder()
        jsonDecoder = JSONDecoder()
    }
}
