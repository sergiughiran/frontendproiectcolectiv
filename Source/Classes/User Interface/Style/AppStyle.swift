//
//  StyleKit.swift
//  project-skeleton
//
//  Created by Botond Magyarosi on 12/03/2017.
//  Copyright © 2017 Halcyon Mobile. All rights reserved.
//

import UIKit

struct AppStyle {

    static func setupAppearance() {
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)]
        
        
        UITabBar.appearance().contentMode = .scaleAspectFit
    }
    
    struct Layout {
        static let padding: CGFloat                 = 20.0
    }
}
