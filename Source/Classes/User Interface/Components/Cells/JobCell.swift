//
//  JobCell.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class JobCell: UITableViewCell {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 24.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var title: String? {
        get {
            return titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    
    var cellDescription: String? {
        get {
            return descriptionLabel.text
        }
        set {
            descriptionLabel.text = newValue
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        
        initContstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initContstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints.append(titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24.0))
        constraints.append(titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24.0))
        constraints.append(titleLabel.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -8.0))
        
        constraints.append(descriptionLabel.topAnchor.constraint(equalTo: centerYAnchor, constant: 8.0))
        constraints.append(descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24.0))
        constraints.append(descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24.0))
        
        NSLayoutConstraint.activate(constraints)
    }
}
