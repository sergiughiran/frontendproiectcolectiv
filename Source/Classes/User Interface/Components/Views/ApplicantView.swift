//
//  ApplicantView.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class ApplicantView: UIView {
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 50.0)
    }
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var applyButton: UIButton = {
        let button = UIButton()
        button.setTitle("View", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setBackgroundImage(UIImage.with(color: UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)), for: .normal)
        button.setBackgroundImage(UIImage.with(color: .lightGray), for: .disabled)
        button.setTitleColor(.darkGray, for: .disabled)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var viewModel: ApplicantViewModel? {
        didSet {
            nameLabel.text = viewModel?.applicant.displayName
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initView()
        initConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initView() {
        addSubview(nameLabel)
        addSubview(applyButton)
        
        applyButton.layer.cornerRadius = 8.0
        applyButton.layer.masksToBounds = true
        applyButton.clipsToBounds = true
        layer.addBorder(edge: .bottom, color: .lightGray, thickness: 0.5)
    }
    
    private func initConstraints() {
        var constraints = [NSLayoutConstraint]()
        
        constraints.append(nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor))
        constraints.append(nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor))
        
        constraints.append(applyButton.trailingAnchor.constraint(equalTo: trailingAnchor))
        constraints.append(applyButton.centerYAnchor.constraint(equalTo: centerYAnchor))
        constraints.append(applyButton.widthAnchor.constraint(equalToConstant: 80.0))
        constraints.append(applyButton.heightAnchor.constraint(equalToConstant: 40.0))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    func addSelector(target: Any?, selector: Selector) {
        applyButton.addTarget(target, action: selector, for: .touchUpInside)
    }
    
    func setAssignee() {
        applyButton.isEnabled = false
    }
}
