//
//  ApplicantViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

class ApplicantViewModel {
    var applicant: User
    
    init(applicant: User) {
        self.applicant = applicant
    }
}
