//
//  OngoingJobsFlowController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 17/01/2019.
//

import UIKit

final class OngoingJobFlowController: NavigationFlowController {
    
    private let jobService: JobService
    
    init(jobService: JobService) {
        self.jobService = jobService
        super.init(from: nil, for: .custom)
    }
    
    required init(from parent: FlowController?, for presentation: FlowControllerPresentation) {
        self.jobService = JobService()
        super.init(from: parent, for: presentation)
    }
    
    override func firstScreen() -> UIViewController {
        let controller = StoryboardScene.Dashboard.ongoingJobsViewController.instantiate()
        controller.viewModel = {
            let viewModel = OngoingJobViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        return controller
    }
}

extension OngoingJobFlowController: OngoingJobViewModelFlowDelegate {
    func getJobs(success: @escaping ([Job]) -> Void) {
        jobService.getMyJobs(success: success, failure: { error in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
    
    func getUser(id: String, success: @escaping (User) -> Void) {
        jobService.getUser(id: id, success: success, failure: { error in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
    
    func jobCellDidTouch(title: String, description: String, creator: String) {
        let controller = StoryboardScene.Dashboard.ongoingJobDetailViewController.instantiate()
        controller.viewModel = {
            let viewModel = OngoingJobDetailViewModel(title: title, description: description, creator: creator)
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
}
