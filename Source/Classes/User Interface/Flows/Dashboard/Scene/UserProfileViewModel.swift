//
//  UserProfileViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import Foundation

protocol UserProfileViewModelFlowDelegate: class {
    func providerSaveButtonDidTouch(email: String?, username: String?, skills: String?, success: @escaping (User) -> Void)
    func clientSaveButtonDidTouch(email: String?, username: String?, success: @escaping (User) -> Void)
    func changePasswordButtonDidTouch()
    func subscribeButtonDidTouch()
    func logOut()
}

class UserProfileViewModel {
    var user: User
    
    weak var flowDelegate: UserProfileViewModelFlowDelegate?
    
    init(user: User) {
        self.user = user
    }
    
    func providerSaveButtonDidTouch(email: String?, username: String?, skills: String?, success: @escaping () -> Void) {
        flowDelegate?.providerSaveButtonDidTouch(email: email, username: username, skills: skills, success: { user in
            self.user = user
            success()
        })
    }
    
    func clientSaveButtonDidTouch(email: String?, username: String?, success: @escaping () -> Void) {
        flowDelegate?.clientSaveButtonDidTouch(email: email, username: username, success: { user in
            self.user = user
            success()
        })
    }
    
    func changePasswordButtonDidTouch() {
        flowDelegate?.changePasswordButtonDidTouch()
    }
    
    func subscribeButtonDidTouch() {
        flowDelegate?.subscribeButtonDidTouch()
    }
    
    func logOut() {
        flowDelegate?.logOut()
    }
}
