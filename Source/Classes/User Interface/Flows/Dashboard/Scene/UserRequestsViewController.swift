//
//  UserRequestsViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import UIKit

final class UserRequestsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: UserRequestsViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let viewModel = viewModel else { return }
        
        viewModel.getRequestedJobs {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 100.0
        
        tableView.register(JobCell.self, forCellReuseIdentifier: "JobCell")
        
        navigationItem.title = "Requests"
        
        viewModel?.getRequestedJobs(success: {
            self.tableView.reloadData()
        })
    }
}

extension UserRequestsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.jobs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as? JobCell else { return UITableViewCell() }
        
        cell.title = viewModel?.jobs?[indexPath.row].title
        cell.cellDescription = viewModel?.jobs?[indexPath.row].title
        return cell
    }
    
    
}

extension UserRequestsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel?.requestCellDidTouch(at: indexPath)
    }
}
