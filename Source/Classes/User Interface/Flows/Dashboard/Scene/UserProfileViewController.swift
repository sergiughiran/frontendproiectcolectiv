//
//  UserProfileViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import UIKit
import RxSwift
import RxCocoa
import RxOptional

final class UserProfileViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var skillsTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var subscribeToNewsletterButton: UIButton!
    
    private let disposeBag = DisposeBag()
    var viewModel: UserProfileViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initBindings()
        signUpForNotifications()
    }
    
    private func initView() {
        saveButton.layer.cornerRadius = 8.0
        saveButton.layer.masksToBounds = true
        saveButton.clipsToBounds = true
        
        let logOutButton = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(logOut))
        navigationItem.rightBarButtonItem = logOutButton
        
        navigationItem.title = "Profile"
        
        scrollView.keyboardDismissMode = .interactive
        
        saveButton.setTitleColor(UIColor.darkGray, for: .disabled)
        saveButton.setBackgroundImage(UIImage.with(color: .lightGray), for: .disabled)
        
        emailTextField.text = viewModel?.user.email
        usernameTextField.text = viewModel?.user.displayName
        
        if let isClient = viewModel?.user.isClient, isClient {
            skillsTextField.isHidden = true
            skillsLabel.isHidden = true
        } else {
            skillsTextField.text = viewModel?.user.skills
        }
    }
    
    private func initBindings() {
        Observable
            .combineLatest(usernameTextField.rx.text.filterNil(), emailTextField.rx.text.filterNil(), skillsTextField.rx.text.filterNil())
            .map { [weak self] (arg0) -> Bool in
                let (username, email, skills) = arg0
                return !username.isEmpty || !email.isEmpty || !skills.isEmpty
            }
            .bind(to: saveButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    private func signUpForNotifications() {
        NotificationCenter.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UITextField.keyboardWillShowNotification, object: nil)
        NotificationCenter.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UITextField.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Foundation.Notification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect, let firstResponder = view.firstResponder else { return }
        let bottomOfFirstResponder = contentView.convert(CGPoint(x: 0, y: firstResponder.frame.height), from: firstResponder).y + 24.0
        let topOfKeyboard = contentView.frame.height - keyboardFrame.height
        
        if bottomOfFirstResponder > topOfKeyboard {
            let offset = bottomOfFirstResponder - topOfKeyboard
            self.scrollView.contentOffset.y = offset
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
        }
        scrollView.alwaysBounceVertical = true
    }
    
    @objc private func keyboardWillHide(_ notification: Foundation.Notification) {
        self.scrollView.contentOffset.y = 0
        self.scrollView.contentInset = UIEdgeInsets.zero
        scrollView.alwaysBounceVertical = false
    }
    
    @objc private func logOut() {
        viewModel?.logOut()
    }
    
    @IBAction func saveButtonDidTouch(_ sender: Any) {
        guard let viewModel = viewModel else { return }
        
        let user: String? = !usernameTextField.text!.isEmpty ? usernameTextField.text : nil
        let email: String? = !emailTextField.text!.isEmpty ? emailTextField.text : nil
        let skills: String? = !skillsTextField.text!.isEmpty ? skillsTextField.text : nil
        
        if viewModel.user.isClient {
            viewModel.clientSaveButtonDidTouch(email: email, username: user, success: {
                self.usernameTextField.text = viewModel.user.displayName
                self.emailTextField.text = viewModel.user.email
            })
        } else {
            viewModel.providerSaveButtonDidTouch(email: email, username: user, skills: skills, success: {
                self.usernameTextField.text = viewModel.user.displayName
                self.emailTextField.text = viewModel.user.email
                self.skillsLabel.text = viewModel.user.skills
            })
        }
    }
    
    
    @IBAction func changePasswordButtonDidTouch(_ sender: Any) {
        viewModel?.changePasswordButtonDidTouch()
    }
    @IBAction func subscribeToNewsletterButtonDidTouch(_ sender: Any) {
        viewModel?.subscribeButtonDidTouch()
    }
}
