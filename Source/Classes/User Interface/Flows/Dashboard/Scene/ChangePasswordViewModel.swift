//
//  ChangePasswordViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import Foundation

protocol ChangePasswordViewModelFlowDelegate: class {
    func updatePassword(oldPassword: String, newPassword: String)
}

class ChangePasswordViewModel {
    weak var flowDelegate: ChangePasswordViewModelFlowDelegate?
    
    func updatePassword(oldPassword: String, newPassword: String) {
        flowDelegate?.updatePassword(oldPassword: oldPassword, newPassword: newPassword)
    }
}
