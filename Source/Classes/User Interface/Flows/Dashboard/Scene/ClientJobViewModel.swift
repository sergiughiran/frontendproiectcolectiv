//
//  ClientJobViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

protocol ClientJobViewModelFlowDelegate: class {
    func viewButtonDidTouch(provider: User, job: Job)
    func getUsers(job: Job)
}

class ClientJobViewModel {
    var job: Job
    var creator: User
    var applicants: [User]?
    var assignee: User?
    var applicantViewModels: [ApplicantViewModel] = []
    
    weak var flowDelegate: ClientJobViewModelFlowDelegate?
    
    init(job: Job, creator: User, applicants: [User]?, asignee: User?) {
        self.job = job
        self.creator = creator
        self.applicants = applicants
        self.assignee = asignee
        if assignee == nil {
            for applicant in applicants! {
                applicantViewModels.append(ApplicantViewModel(applicant: applicant))
            }
        } else {
            applicantViewModels.append(ApplicantViewModel(applicant: assignee!))
        }
    }
    
    func viewButtonDidTouch(provider: User) {
        flowDelegate?.viewButtonDidTouch(provider: provider, job: job)
    }
    
    func getUsers() {
        flowDelegate?.getUsers(job: job)
    }
}
