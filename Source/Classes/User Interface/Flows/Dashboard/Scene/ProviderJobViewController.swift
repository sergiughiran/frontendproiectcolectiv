//
//  ProviderJobViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

class ProviderJobViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
    
    var viewModel: ProviderJobViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {        
        applyButton.layer.cornerRadius = 8.0
        applyButton.layer.masksToBounds = true
        applyButton.clipsToBounds = true
        
        titleLabel.text = viewModel?.job.title
        descriptionLabel.text = viewModel?.job.description
        creatorLabel.text = viewModel?.creator.displayName
        
        applyButton.setTitleColor(.darkGray, for: .disabled)
        applyButton.setBackgroundImage(UIImage.with(color: .lightGray), for: .disabled)
        
        if viewModel?.job.assignee != nil {
            applyButton.isEnabled = false
            
            navigationItem.title = "Job already assigned"
        } else {
            navigationItem.title = viewModel?.job.title
        }
    }
    @IBAction func applyButtonDIdTouch(_ sender: Any) {
        viewModel?.apply()
    }
    
}
