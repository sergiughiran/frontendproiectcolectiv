//
//  JobListViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

protocol JobListViewModelFlowDelegate: class {
    func getAllJobs(success: @escaping ([Job]) -> Void)
    func getMyJobs(success: @escaping ([Job]) -> Void)
    func jobCellDidSelect(id: String, job: Job, isClient: Bool)
    func addJobButtonDidTouch()
}

class JobListViewModel {
    
    var jobs: [Job]?
    var isClient: Bool {
        let currentUser = UserDefaultsManager.shared.user!
        
        return currentUser.isClient
    }
    weak var flowDelegate: JobListViewModelFlowDelegate?
    
    func getJobs(success: @escaping () -> Void) {
        
        if !isClient{
            flowDelegate?.getAllJobs(success: { (jobs) in
                self.jobs = jobs
                success()
            })
        } else if isClient {
            flowDelegate?.getMyJobs(success: { (jobs) in
                self.jobs = jobs
                success()
            })
        }
    }
    
    func jobCellDidSelect(at indexPath: IndexPath) {
        guard let id = jobs?[indexPath.row]._creator, let job = jobs?[indexPath.row] else { return }
        flowDelegate?.jobCellDidSelect(id: id, job: job, isClient: isClient)
    }
    
    func addJobButtonDidTouch() {
        flowDelegate?.addJobButtonDidTouch()
    }
}
