//
//  JobListViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class JobListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: JobListViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let viewModel = viewModel else { return }
        viewModel.getJobs(success: {
            self.tableView.reloadData()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {        
        navigationItem.title = "Jobs"
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = 100.0
        tableView.tableFooterView = UIView()
        
        tableView.register(JobCell.self, forCellReuseIdentifier: "JobCell")
        viewModel?.getJobs(success: {
            self.tableView.reloadData()
        })
        
        if let viewModel = viewModel, viewModel.isClient {
            let addJobBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addJobButtonDidTouch))
            navigationItem.rightBarButtonItem = addJobBarButton
        }
    }
    
    @objc private func addJobButtonDidTouch() {
        viewModel?.addJobButtonDidTouch()
    }
}

extension JobListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.jobs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as? JobCell else { return UITableViewCell() }
        
        cell.title = viewModel?.jobs?[indexPath.row].title
        cell.cellDescription = viewModel?.jobs?[indexPath.row].description
        
        return cell
    }
    
    
}

extension JobListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        hidesBottomBarWhenPushed = true
        
        viewModel?.jobCellDidSelect(at: indexPath)
        hidesBottomBarWhenPushed = false
    }
}
