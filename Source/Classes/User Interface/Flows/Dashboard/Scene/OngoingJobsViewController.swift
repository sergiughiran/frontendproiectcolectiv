//
//  OngoingJobsViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 17/01/2019.
//

import UIKit

final class OngoingJobsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: OngoingJobViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let viewModel = viewModel else { return }
        
        viewModel.getJobs(success: {
            self.tableView.reloadData()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        tableView.tableFooterView = UIView()
        
        tableView.rowHeight = 100.0
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(JobCell.self, forCellReuseIdentifier: "JobCell")
        navigationItem.title = "Ongoing Jobs"
        
        viewModel?.getJobs(success: {
            self.tableView.reloadData()
        })
    }
    
}

extension OngoingJobsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.jobs?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as? JobCell else { return UITableViewCell() }
        cell.title = viewModel?.jobs?[indexPath.row].title
        cell.cellDescription = viewModel?.jobs?[indexPath.row].description
        return cell
    }
}

extension OngoingJobsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        viewModel?.jobCellDidTouch(at: indexPath)
    }
}
