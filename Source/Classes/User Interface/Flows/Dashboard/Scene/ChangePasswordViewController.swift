//
//  ChangePasswordViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import UIKit
import RxSwift
import RxCocoa
import RxOptional

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var viewModel: ChangePasswordViewModel?
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initBindings()
    }
    
    private func initView() {
        saveButton.layer.cornerRadius = 8.0
        saveButton.layer.masksToBounds = true
        saveButton.clipsToBounds = true
    }
    
    private func initBindings() {
        Observable
            .combineLatest(oldPasswordTextField.rx.text.filterNil(), newPasswordTextField.rx.text.filterNil())
            .throttle(0.3, scheduler: MainScheduler.instance)
            .map { (arg0) -> Bool in
                let (oldPassword, newPassword) = arg0
                return !oldPassword.isEmpty && !newPassword.isEmpty
            }
            .bind(to: saveButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    @IBAction func saveButtonDidTouch(_ sender: Any) {
        viewModel?.updatePassword(oldPassword: oldPasswordTextField.text!, newPassword: newPasswordTextField.text!)
    }
}
