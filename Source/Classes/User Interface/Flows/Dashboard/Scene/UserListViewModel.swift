//
//  UserListViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import Foundation

protocol UserListViewModelFlowDelegate: class {
    func getUsers(success: @escaping ([User]) -> Void)
    func userCellDidTouch(with user: User, job: Job)
}

final class UserListViewModel {
    
    var users: [User]
    var job: Job
    weak var flowDelegate: UserListViewModelFlowDelegate?
    
    init(users: [User], job: Job) {
        self.users = users.filter { return job._id != $0._id }
        self.job = job
    }
    
    func getUsers(success: @escaping () -> Void) {
        flowDelegate?.getUsers(success: { (users) in
            self.users = users.filter { return self.job._id != $0._id }
            success()
        })
    }
    
    func userCellDidTouch(at indexPath: IndexPath) {
        flowDelegate?.userCellDidTouch(with: users[indexPath.row], job: job)
    }
}
