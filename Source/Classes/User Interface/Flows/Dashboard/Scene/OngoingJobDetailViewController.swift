//
//  OngoingJobDetailViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 17/01/2019.
//

import UIKit

final class OngoingJobDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    
    var viewModel: OngoingJobDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        titleLabel.text = viewModel?.title
        descriptionLabel.text = viewModel?.description
        creatorLabel.text = viewModel?.creator
        
        navigationItem.title = viewModel?.title
    }
}
