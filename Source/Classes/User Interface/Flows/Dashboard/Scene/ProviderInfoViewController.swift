//
//  ProviderInfoViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import UIKit

final class ProviderInfoViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    
    var viewModel: ProviderInfoViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        acceptButton.layer.cornerRadius = 8.0
        
        emailLabel.text = viewModel?.provider.email
        usernameLabel.text = viewModel?.provider.displayName
        skillsLabel.text = viewModel?.provider.skills
    }
    
    
    @IBAction func acceptButtonDidTouch(_ sender: Any) {
        viewModel?.assignButtonDidTouch()
    }
    
}
