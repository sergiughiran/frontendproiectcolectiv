//
//  UserDetailViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import Foundation

protocol UserDetailViewModelFlowDelegate: class {
    func requestUser(userID: String, jobID: String)
}

final class UserDetailViewModel {
    var user: User
    var job: Job
    
    weak var flowDelegate: UserDetailViewModelFlowDelegate?
    
    init(user: User, job: Job) {
        self.user = user
        self.job = job
    }
    
    func requestUser() {
        flowDelegate?.requestUser(userID: user._id, jobID: job._id)
    }
}
