//
//  AddJobViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import UIKit

final class AddJobViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var viewModel: AddJobViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        signUpForNotifications()
    }
    
    private func initView() {
        saveButton.layer.cornerRadius = 8.0
        saveButton.layer.masksToBounds = true
        saveButton.clipsToBounds = true
        
        scrollView.keyboardDismissMode = .interactive
    }
    
    private func signUpForNotifications() {
        NotificationCenter.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UITextField.keyboardWillShowNotification, object: nil)
        NotificationCenter.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UITextField.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Foundation.Notification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
        let bottomOfStackView = contentView.convert(CGPoint(x: 0, y: stackView.frame.height), from: stackView).y + 24.0
        let topOfKeyboard = contentView.frame.height - keyboardFrame.height
        
        if bottomOfStackView > topOfKeyboard {
            let offset = bottomOfStackView - topOfKeyboard
            self.scrollView.contentOffset.y = offset
        }
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
        scrollView.alwaysBounceVertical = true
    }
    
    @objc private func keyboardWillHide(_ notification: Foundation.Notification) {
        self.scrollView.contentOffset.y = 0
        self.scrollView.contentInset = UIEdgeInsets.zero
        scrollView.alwaysBounceVertical = false
    }
    
    @IBAction func saveButtonDidTouch(_ sender: Any) {
        guard let title = titleTextField.text, let description = descriptionTextField.text, !title.isEmpty, !description.isEmpty else { return }
        viewModel?.saveButtonDidTouch(title: title, description: description)
    }
}
