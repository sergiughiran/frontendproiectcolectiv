//
//  ProviderRequestViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import Foundation

protocol ProviderRequestViewModelFlowDelegate: class {
    func acceptRequest(userID: String, jobID: String)
}

final class ProviderRequestViewModel {
    var job: Job
    weak var flowDelegate: ProviderRequestViewModelFlowDelegate?
    
    
    init(job: Job) {
        self.job = job
    }
    
    func acceptRequest() {
        guard let userID = UserDefaultsManager.shared.user?._id else { return }
        flowDelegate?.acceptRequest(userID: userID, jobID: job._id)
    }
}
