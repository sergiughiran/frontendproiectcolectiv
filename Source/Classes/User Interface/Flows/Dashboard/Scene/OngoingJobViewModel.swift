//
//  OngoingJobViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 17/01/2019.
//

import Foundation

protocol OngoingJobViewModelFlowDelegate: class {
    func getJobs(success: @escaping ([Job]) -> Void)
    func jobCellDidTouch(title: String, description: String, creator: String)
    func getUser(id: String, success: @escaping (User) -> Void)
}

class OngoingJobViewModel {
    
    var jobs: [Job]?
    weak var flowDelegate: OngoingJobViewModelFlowDelegate?
    
    func getJobs(success: @escaping () -> Void) {
        flowDelegate?.getJobs(success: { (jobs) in
            self.jobs = jobs
            success()
        })
    }
    
    func jobCellDidTouch(at indexPath: IndexPath) {
        guard let job = jobs?[indexPath.row] else { return }
        
        flowDelegate?.getUser(id: job._creator, success: { (user) in
            self.flowDelegate?.jobCellDidTouch(title: job.title, description: job.description, creator: user.displayName)
        })
    }
}
