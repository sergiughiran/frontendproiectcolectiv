//
//  AddJobViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import Foundation

protocol AddJobViewModelFlowDelegate: class {
    func saveButtonDidTouch(title: String, description: String)
}

class AddJobViewModel {
    weak var flowDelegate: AddJobViewModelFlowDelegate?
    
    func saveButtonDidTouch(title: String, description: String) {
        flowDelegate?.saveButtonDidTouch(title: title, description: description)
    }
}
