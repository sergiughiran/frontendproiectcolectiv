//
//  ClientJobViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

class ClientJobViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    @IBOutlet weak var applicantsLabel: UILabel!
    @IBOutlet weak var applicantsTitleLabel: UILabel!
    @IBOutlet weak var requestButton: UIButton!
    
    var viewModel: ClientJobViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        
        requestButton.layer.cornerRadius = 8.0
        requestButton.layer.masksToBounds = true
        requestButton.clipsToBounds = true
        
        requestButton.setBackgroundImage(UIImage.with(color: .lightGray), for: .disabled)
        requestButton.setTitleColor(.darkGray, for: .disabled)
        
        guard let viewModel = viewModel else { return }
        
        titleLabel.text = viewModel.job.title
        descriptionLabel.text = viewModel.job.description
        creatorLabel.text = viewModel.creator.displayName
        
        navigationItem.title = viewModel.job.title
        
        if let applicants = viewModel.applicants, applicants.count > 0 {
            applicantsLabel.isHidden = true
            requestButton.isEnabled = true
            for i in 0..<applicants.count {
                insertApplicantView(at: i)
            }
        } else if let asignee = viewModel.assignee {
            applicantsLabel.isHidden = true
            applicantsTitleLabel.text = "Assignee"
            insertAssigneeView()
            requestButton.isEnabled = false
        }
    }
    
    private func insertApplicantView(at position: Int) {
        let applicantView = ApplicantView()
        applicantView.viewModel = viewModel?.applicantViewModels[position]
        applicantView.addSelector(target: self, selector: #selector(acceptButtonDidTouch(_:)))
        stackView.addArrangedSubview(applicantView)
    }
    
    private func insertAssigneeView() {
        let applicantView = ApplicantView()
        applicantView.viewModel = viewModel?.applicantViewModels[0]
        applicantView.setAssignee()
        stackView.addArrangedSubview(applicantView)
    }
    
    @objc private func acceptButtonDidTouch(_ sender: UIButton) {
        guard let applicantView = sender.superview as? ApplicantView, let applicant =  applicantView.viewModel?.applicant else { return }
        
        viewModel?.viewButtonDidTouch(provider: applicant)
    }
    
    
    @IBAction func requestButtonDidTouch(_ sender: Any) {
        viewModel?.getUsers()
    }
}
