//
//  OngoingJobDetailViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 17/01/2019.
//

import Foundation

class OngoingJobDetailViewModel {
    var title: String
    var description: String
    var creator: String
    
    init(title: String, description: String, creator: String) {
        self.title = title
        self.description = description
        self.creator = creator
    }
}
