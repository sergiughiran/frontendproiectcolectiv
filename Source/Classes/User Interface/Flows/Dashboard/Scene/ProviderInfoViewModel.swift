//
//  ProviderInfoViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import Foundation

protocol ProviderInfoViewModelFlowDelegate: class {
    func assignJobToUser(jobID: String, userID: String)
}

class ProviderInfoViewModel {
    
    var provider: User
    var job: Job
    
    weak var flowDelegate: ProviderInfoViewModelFlowDelegate?
    
    init(provider: User, job: Job) {
        self.provider = provider
        self.job = job
    }
    
    func assignButtonDidTouch() {
        flowDelegate?.assignJobToUser(jobID: job._id, userID: provider._id)
    }
}
