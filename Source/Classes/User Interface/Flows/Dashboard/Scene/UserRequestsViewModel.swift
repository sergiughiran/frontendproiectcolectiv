//
//  UserRequestsViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import Foundation

protocol UserRequestsViewModelFlowDelegate: class {
    func requestCellDidTouch(with job: Job)
    func getRequestedJobs(success: @escaping ([Job]) -> Void)
}

final class UserRequestsViewModel {
    var jobs: [Job]?
    weak var flowDelegate: UserRequestsViewModelFlowDelegate?
    
    func getRequestedJobs(success: @escaping () -> Void) {
        flowDelegate?.getRequestedJobs(success: { (jobs) in
            self.jobs = jobs
            success()
        })
    }
    
    func requestCellDidTouch(at indexPath: IndexPath) {
        flowDelegate?.requestCellDidTouch(with: (jobs?[indexPath.row])!)
    }
}
