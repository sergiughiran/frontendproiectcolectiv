//
//  UserDetailViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import UIKit

final class UserDetailViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var requestButton: UIButton!
    
    var viewModel: UserDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        requestButton.layer.cornerRadius = 8.0
        requestButton.layer.masksToBounds = true
        requestButton.clipsToBounds = true
        
        emailLabel.text = viewModel?.user.email
        usernameLabel.text = viewModel?.user.displayName
        skillsLabel.text = viewModel?.user.skills
    }
    
    @IBAction func requestButtonDidTouch(_ sender: Any) {
        viewModel?.requestUser()
    }
}
