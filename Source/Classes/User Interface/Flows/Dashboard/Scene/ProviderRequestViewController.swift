//
//  ProviderRequestViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import UIKit

final class ProviderRequestViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var creatorLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    
    var viewModel: ProviderRequestViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        titleLabel.text = viewModel?.job.title
        descriptionLabel.text = viewModel?.job.description
        creatorLabel.text = viewModel?.job._creator
        
        acceptButton.layer.cornerRadius = 8.0
        acceptButton.layer.masksToBounds = true
        acceptButton.clipsToBounds = true
    }
    
    @IBAction func acceptButtonDidTouch(_ sender: Any) {
        viewModel?.acceptRequest()
    }
}
