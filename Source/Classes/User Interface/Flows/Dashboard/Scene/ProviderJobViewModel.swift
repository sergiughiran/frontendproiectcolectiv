//
//  ProviderJobViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

protocol ProviderJobViewModelFlowDelegate: class {
    func apply(jobID: String, creator: User)
}

class ProviderJobViewModel {
    var creator: User
    var job: Job
    
    weak var flowDelegate: ProviderJobViewModelFlowDelegate?
    
    init(creator: User, job: Job) {
        self.creator = creator
        self.job = job
    }
    
    func apply() {
        flowDelegate?.apply(jobID: job._id, creator: creator)
    }
}
