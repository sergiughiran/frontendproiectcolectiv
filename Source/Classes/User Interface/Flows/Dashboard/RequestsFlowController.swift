//
//  RequestsFlowController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 18/01/2019.
//

import UIKit

final class RequestsFlowController: NavigationFlowController {
    
    private var jobService: JobService
    
    init(jobService: JobService) {
        self.jobService = jobService
        super.init(from: nil, for: .custom)
    }
    
    required init(from parent: FlowController?, for presentation: FlowControllerPresentation) {
        self.jobService = JobService()
        super.init(from: parent, for: presentation)
    }
    
    override func firstScreen() -> UIViewController {
        let controller = StoryboardScene.Dashboard.userRequestsViewController.instantiate()
        controller.viewModel = {
            let viewModel = UserRequestsViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        return controller
    }
    
}

extension RequestsFlowController: UserRequestsViewModelFlowDelegate {
    func getRequestedJobs(success: @escaping ([Job]) -> Void) {
        jobService.getUserRequests(success: success, failure: { error in
            self.showMessage(title: "Error!", message: "Something went wrong, please try again!")
        })
    }
    
    func requestCellDidTouch(with job: Job) {
        let controller = StoryboardScene.Dashboard.providerRequestViewController.instantiate()
        controller.viewModel = {
            let viewModel = ProviderRequestViewModel(job: job)
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension RequestsFlowController: ProviderRequestViewModelFlowDelegate {
    func acceptRequest(userID: String, jobID: String) {
        jobService.acceptRequest(userID: userID, jobID: jobID, success: { (job) in
            self.navigationController?.popViewController()
        }, failure: { error in
            self.showMessage(title: "Error!", message: "Something went wrong, please try again!")
        })
    }
}
