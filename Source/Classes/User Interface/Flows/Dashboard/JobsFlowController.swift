//
//  JobsFlowController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class JobsFlowController: NavigationFlowController {
    
    private var jobService: JobService
    
    init(jobService: JobService) {
        self.jobService = jobService
        super.init(from: nil, for: .custom)
    }
    
    required init(from parent: FlowController?, for presentation: FlowControllerPresentation) {
        jobService = JobService()
        super.init(from: parent, for: presentation)
    }
    
    override func firstScreen() -> UIViewController {
        let controller = StoryboardScene.Dashboard.jobListViewController.instantiate()
        controller.viewModel = {
            let viewModel = JobListViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        return controller
    }
}

extension JobsFlowController: JobListViewModelFlowDelegate {
    func getAllJobs(success: @escaping ([Job]) -> Void) {
        jobService.getJobList(success: success) { (error) in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        }
    }
    
    func getMyJobs(success: @escaping ([Job]) -> Void) {
        jobService.getMyJobs(success: success) { (error) in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        }
    }
    
    func jobCellDidSelect(id: String, job: Job, isClient: Bool) {
        jobService.getUser(id: id, success: { (user) in
            if !isClient {
                let controller = StoryboardScene.Dashboard.providerJobViewController.instantiate()
                controller.viewModel = {
                    let viewModel = ProviderJobViewModel(creator: user, job: job)
                    viewModel.flowDelegate = self
                    return viewModel
                }()
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                if job.assignee != nil {
                    self.jobService.getUser(id: job.assignee!, success: { (asignee) in
                        let controller = StoryboardScene.Dashboard.clientJobViewController.instantiate()
                        controller.viewModel = {
                            let viewModel = ClientJobViewModel(job: job, creator: user, applicants: nil, asignee: asignee)
                            viewModel.flowDelegate = self
                            return viewModel
                        }()
                        self.navigationController?.pushViewController(controller, animated: true)
                    }, failure: { (error) in
                        self.showMessage(title: "Error", message: "Something went wrong, please try again!")
                    })
                } else {
                    self.jobService.getApplicants(id: job._id, success: { (applicants) in
                        let controller = StoryboardScene.Dashboard.clientJobViewController.instantiate()
                        controller.viewModel = {
                            let viewModel = ClientJobViewModel(job: job, creator: user, applicants: applicants, asignee: nil)
                            viewModel.flowDelegate = self
                            return viewModel
                        }()
                        self.navigationController?.pushViewController(controller, animated: true)
                    }, failure: { (error) in
                        self.showMessage(title: "Error", message: "Something went wrong, please try again!")
                    })
                }
            }
        }, failure: { (error) in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
    
    func addJobButtonDidTouch() {
        let controller = StoryboardScene.Dashboard.addJobViewController.instantiate()
        controller.viewModel = {
            let viewModel = AddJobViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension JobsFlowController: ClientJobViewModelFlowDelegate {
    func viewButtonDidTouch(provider: User, job: Job) {
        let controller = StoryboardScene.Dashboard.providerInfoViewController.instantiate()
        controller.viewModel = {
            let viewModel = ProviderInfoViewModel(provider: provider, job: job)
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func getUsers(job: Job) {
        jobService.getUsers(success: { (users) in
            let controller = StoryboardScene.Dashboard.userListViewController.instantiate()
            controller.viewModel = {
                let viewModel = UserListViewModel(users: users, job: job)
                viewModel.flowDelegate = self
                return viewModel
            }()
            self.navigationController?.pushViewController(controller, animated: true)
        }, failure: { error in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
}

extension JobsFlowController: ProviderInfoViewModelFlowDelegate {
    func assignJobToUser(jobID: String, userID: String) {
        jobService.assignJob(jobID: jobID, userID: userID, success: { (job) in
            self.navigationController?.popViewController()

            let controller = StoryboardScene.Dashboard.jobListViewController.instantiate()
            controller.viewModel = {
                let viewModel = JobListViewModel()
                viewModel.flowDelegate = self
                return viewModel
            }()

            self.navigationController?.setViewControllers([controller], animated: true)
        }, failure: { (error) in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
}

extension JobsFlowController: ProviderJobViewModelFlowDelegate {
    func apply(jobID: String, creator: User) {
        jobService.apply(jobID: jobID, success: { (job) in
            self.showMessage(title: "Success", message: "Application successful!")
            self.navigationController?.popViewController()
            
            let controller = StoryboardScene.Dashboard.providerJobViewController.instantiate()
            controller.viewModel = {
                let viewModel = ProviderJobViewModel(creator: creator, job: job)
                return viewModel
            }()
            self.navigationController?.pushViewController(controller, animated: true)
            
        }, failure: { error in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
}

extension JobsFlowController: AddJobViewModelFlowDelegate {
    func saveButtonDidTouch(title: String, description: String) {
        jobService.addJob(title: title, description: description, success: { (job) in
            self.showMessage(title: "Success", message: "The job was added successfully!")
            self.navigationController?.popViewController()
            
            let controller = StoryboardScene.Dashboard.jobListViewController.instantiate()
            controller.viewModel = {
                let viewModel = JobListViewModel()
                viewModel.flowDelegate = self
                return viewModel
            }()
            
            self.navigationController?.setViewControllers([controller], animated: true)
            
        }, failure: { error in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
}

extension JobsFlowController: UserListViewModelFlowDelegate {
    func userCellDidTouch(with user: User, job: Job) {
        let controller = StoryboardScene.Dashboard.userDetailViewController.instantiate()
        controller.viewModel = {
            let viewModel = UserDetailViewModel(user: user, job: job)
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func getUsers(success: @escaping ([User]) -> Void) {
        jobService.getUsers(success: success, failure: { error in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
}

extension JobsFlowController: UserDetailViewModelFlowDelegate {
    func requestUser(userID: String, jobID: String) {
        jobService.requestUser(userID: userID, jobID: jobID, success: { (_) in
            self.navigationController?.popViewController()
        }, failure: { error in
            self.showMessage(title: "Error", message: "Something went wrong, please try again!")
        })
    }
}
