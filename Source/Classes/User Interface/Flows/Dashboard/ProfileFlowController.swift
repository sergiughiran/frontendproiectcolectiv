//
//  ProfileFlowController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import UIKit

final class ProfileFlowController: NavigationFlowController {
    private var user: User {
        return UserDefaultsManager.shared.user!
    }
    
    private var jobService: JobService
    private var authenticationService: AuthenticationService
    
    init(jobService: JobService, authenticationService: AuthenticationService) {
        self.jobService = jobService
        self.authenticationService = authenticationService
        super.init(from: nil, for: .custom)
    }
    
    required init(from parent: FlowController?, for presentation: FlowControllerPresentation) {
        self.jobService = JobService()
        self.authenticationService = AuthenticationService()
        super.init(from: parent, for: presentation)
    }
    
    override func firstScreen() -> UIViewController {
        let controller = StoryboardScene.Dashboard.userProfileViewController.instantiate()
        controller.viewModel = {
            let viewModel = UserProfileViewModel(user: user)
            viewModel.flowDelegate = self
            return viewModel
        }()
        return controller
    }
}

extension ProfileFlowController: UserProfileViewModelFlowDelegate {
    func providerSaveButtonDidTouch(email: String?, username: String?, skills: String?, success: @escaping (User) -> Void) {
        var newUser = user
        if email != nil {
            newUser.email = email!
        }
        
        if username != nil {
            newUser.displayName = username!
        }
        
        if skills != nil {
            newUser.skills = skills
        }
        
        jobService.updateProvider(email: newUser.email, username: newUser.displayName, skills: newUser.skills, isClient: newUser.isClient, success: { (user) in
            self.showMessage(title: "Success", message: "Profile update was successfull!")
            success(user)
        }, failure: { error in
            self.showMessage(title: "Error!", message: "Something went wrong, please try again!")
        })
    }
    
    func clientSaveButtonDidTouch(email: String?, username: String?, success: @escaping (User) -> Void) {
        var newUser = user
        if email != nil {
            newUser.email = email!
        }
        
        if username != nil {
            newUser.displayName = username!
        }
        
        jobService.updateClient(email: newUser.email, username: newUser.displayName, isClient: newUser.isClient, success: { (user) in
            self.showMessage(title: "Success", message: "Profile update was successfull!")
            success(user)
        }, failure: { error in
            self.showMessage(title: "Error!", message: "Something went wrong, please try again!")

        })
    }
    
    func changePasswordButtonDidTouch() {
        let controller = StoryboardScene.Dashboard.changePasswordViewController.instantiate()
        controller.viewModel = {
            let viewModel = ChangePasswordViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func subscribeButtonDidTouch() {
        jobService.subscribeToNewsletter(success: { (_) in
            self.showMessage(title: "Success!", message: "Subscribe was successful!")
        }, failure: { error in
            self.showMessage(title: "Error!", message: "Something went wrong, please try again!")
        })
    }
    
    func logOut() {
        authenticationService.logOut(success: { (_) in
            self.authenticationService.isAuthenticated.onNext(false)
        }, failure: { error in
            self.showMessage(title: "Error!", message: "Something went wrong, please try again!")
        })
    }
}

extension ProfileFlowController: ChangePasswordViewModelFlowDelegate {
    func updatePassword(oldPassword: String, newPassword: String) {
        jobService.updatePassword(oldPassword: oldPassword, newPassword: newPassword, success: { (user) in
            self.showMessage(title: "Success", message: "Password update was successfull!")
            self.navigationController?.popViewController()
        }, failure: { error in
            self.showMessage(title: "Error!", message: "Something went wrong, please try again!")
        })
    }
}
