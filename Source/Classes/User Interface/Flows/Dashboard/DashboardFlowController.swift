//
//  DashboardFlowController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class DashboardFlowController: FlowController {
    
    private var jobService: JobService
    private var jobsFlow: JobsFlowController?
    private var profileFlow: ProfileFlowController?
    private var ongoingJobFlow: OngoingJobFlowController?
    private var requestsFlow: RequestsFlowController?
    private var authenticationService: AuthenticationService
    
    
    var mainViewController: UIViewController? {
        return tabBarController
    }
    var flowPresentation: FlowControllerPresentation = .custom
    var parentFlow: FlowController? = nil
    
    private var tabBarController: UITabBarController?
    
    init(jobService: JobService, authenticationService: AuthenticationService) {
        self.jobService = jobService
        self.authenticationService = authenticationService
    }
    
    init(from parent: FlowController?, for presentation: FlowControllerPresentation) {
        self.parentFlow = parent
        self.flowPresentation = presentation
        self.jobService = JobService()
        self.authenticationService = AuthenticationService()
    }
    
    func initMainViewController() {
        guard tabBarController == nil else { return }
        
        tabBarController = UITabBarController()
        
        var viewControllers = [UIViewController]()
        
        jobsFlow = JobsFlowController(jobService: jobService)
        jobsFlow?.start(customPresentation: { (viewController) in
            viewController.tabBarItem.title = "Jobs"
            viewController.tabBarItem.image = UIImage(named: "icon_job")
        })
        
        profileFlow = ProfileFlowController(jobService: jobService, authenticationService: authenticationService)
        profileFlow?.start(customPresentation: { (viewController) in
            viewController.tabBarItem.title = "Profile"
            viewController.tabBarItem.image = UIImage(named: "icon_profile")
        })
        
        ongoingJobFlow = OngoingJobFlowController(jobService: jobService)
        ongoingJobFlow?.start(customPresentation: { (viewController) in
            viewController.tabBarItem.title = "Ongoing"
            viewController.tabBarItem.image = UIImage(named: "icon_clock")
        })
        
        requestsFlow = RequestsFlowController(jobService: jobService)
        requestsFlow?.start(customPresentation: { (viewController) in
            viewController.tabBarItem.title = "Requests"
            viewController.tabBarItem.image = UIImage(named: "icon_profile")
        })
        
        guard let jobsViewController = jobsFlow?.mainViewController,
            let profileViewController = profileFlow?.mainViewController,
            let ongoingJobViewController = ongoingJobFlow?.mainViewController,
            let requestsViewController = requestsFlow?.mainViewController
        else { return }
        
        viewControllers.append(jobsViewController)
        
        if let user = UserDefaultsManager.shared.user, !user.isClient {
            viewControllers.append(ongoingJobViewController)
            viewControllers.append(requestsViewController)
        }
        
        viewControllers.append(profileViewController)
        
        tabBarController?.setViewControllers(viewControllers, animated: true)
        tabBarController?.selectedIndex = 0
    }
    
    func firstScreen() -> UIViewController {
        guard let tabBarController = tabBarController else {
            fatalError("Tab bar controller not initialized")
        }
        
        return tabBarController
    }
    
    
}
