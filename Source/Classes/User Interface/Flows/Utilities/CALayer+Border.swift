//
//  CALayer+Border.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import UIKit

extension CALayer {
    
    /// Add a border on an edge of a layer.
    ///
    /// - Parameters:
    ///   - edge: The edge of the layer to add the border to.
    ///   - color: The color of the border.
    ///   - thickness: The thickness of the border.
    ///
    /// - Warning: Be sure to call this in **layoutSubviews()** or **viewDidLayoutSubviews()** in order to have a valid frame.
    ///
    /// - Returns: The newly added border.
    @discardableResult
    func addBorder(edge: UIRectEdge, color: UIColor?, thickness: CGFloat) -> CALayer {
        let border = CALayer()
        border.backgroundColor = color?.cgColor
        
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        
        addSublayer(border)
        return border
    }
}
