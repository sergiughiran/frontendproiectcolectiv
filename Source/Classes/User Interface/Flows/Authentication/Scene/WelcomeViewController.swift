//
//  WelcomeViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var registerAsClientButton: UIButton!
    @IBOutlet weak var registerAsProviderButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    
    var viewModel: WelcomeViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    private func initView() {
        
        registerAsClientButton.layer.cornerRadius = 8.0
        registerAsProviderButton.layer.cornerRadius = 8.0
    }
    
    
    @IBAction func registerAsClientButtonDidTouch(_ sender: Any) {
        viewModel?.registerClientButtonDidTouch()
    }
    @IBAction func registerAsProviderButtonDidTouch(_ sender: Any) {
        viewModel?.registerProviderButtonDidTouch()
    }
    @IBAction func logInButtonDidTouch(_ sender: Any) {
        viewModel?.logInButtonDidTouch()
    }
    
    
}
