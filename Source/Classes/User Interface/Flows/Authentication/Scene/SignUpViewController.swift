//
//  SignUpViewController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class SignUpViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var skillsTextView: UITextView!
    @IBOutlet weak var displayNameTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    var viewModel: SignUpViewModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.barTintColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        signUpForNotifications()
    }
    
    private func initView() {
        navigationItem.title = "Sign Up"
        
        skillsTextView.layer.borderColor = UIColor.lightGray.cgColor
        skillsTextView.layer.borderWidth = 0.2
        skillsTextView.layer.cornerRadius = 8.0
        
        skillsTextView.text = "Skills"

        signUpButton.layer.cornerRadius = 8.0
        
        scrollView.keyboardDismissMode = .interactive
    }
    
    private func signUpForNotifications() {
        NotificationCenter.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UITextField.keyboardWillShowNotification, object: nil)
        NotificationCenter.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UITextField.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Foundation.Notification) {
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect, let firstResponder = view.firstResponder else { return }
        let bottomOfFirstResponder = contentView.convert(CGPoint(x: 0, y: firstResponder.frame.height), from: firstResponder).y + 24.0
        let topOfKeyboard = contentView.frame.height - keyboardFrame.height
        
        if bottomOfFirstResponder > topOfKeyboard {
            let offset = bottomOfFirstResponder - topOfKeyboard
            self.scrollView.contentOffset.y = offset
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height, right: 0)
        }
        scrollView.alwaysBounceVertical = true
    }
    
    @objc private func keyboardWillHide(_ notification: Foundation.Notification) {
        self.scrollView.contentOffset.y = 0
        self.scrollView.contentInset = UIEdgeInsets.zero
        scrollView.alwaysBounceVertical = false
    }
    
    
    @IBAction func singUpButtonDidTouch(_ sender: Any) {
        guard let email = emailTextField.text, let password = passwordTextField.text, let displayName = displayNameTextField.text, !email.isEmpty, !password.isEmpty, !displayName.isEmpty else { return }
        
        viewModel?.signUp(email: email, password: password, skills: skillsTextView.text, displayName: displayName)
    }
}
