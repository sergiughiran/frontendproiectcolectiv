//
//  SignUpViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 13/01/2019.
//

import Foundation

protocol SignUpViewModelFlowDelegate: class {
    func signUp(email: String, password: String, skills: String?, displayName: String)
}

class SignUpViewModel {
    weak var flowDelegate: SignUpViewModelFlowDelegate?
    
    func signUp(email: String, password: String, skills: String?, displayName: String) {
        flowDelegate?.signUp(email: email, password: password, skills: skills, displayName: displayName)
    }
}
