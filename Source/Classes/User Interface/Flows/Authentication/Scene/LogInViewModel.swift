//
//  LogInViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

protocol LogInViewModelFlowDelegate: class {
    func loginButtonDidTouch(email: String, password: String, success: @escaping (User) -> Void)
}

class LogInViewModel {
    
    weak var flowDelegate: LogInViewModelFlowDelegate?
    
    func logInButtonDidTouch(email: String, password: String) {
        flowDelegate?.loginButtonDidTouch(email: email, password: password, success: { user in
            print("Success")
        })
    }
}
