//
//  WelcomeViewModel.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import Foundation

protocol WelcomeViewModelFlowDelegate: class {
    func registerClientButtonDidTouch()
    func registerProviderButtonDidTouch()
    func logInButtonDidTouch()
}

protocol WelcomeViewModel {
    var flowDelegate: WelcomeViewModelFlowDelegate? { get set }
    
    func registerClientButtonDidTouch()
    func registerProviderButtonDidTouch()
    func logInButtonDidTouch()
}

class WelcomeViewModelImpl: WelcomeViewModel {
    
    weak var flowDelegate: WelcomeViewModelFlowDelegate?
    
    func registerClientButtonDidTouch() {
        flowDelegate?.registerClientButtonDidTouch()
    }
    
    func registerProviderButtonDidTouch(){
        flowDelegate?.registerProviderButtonDidTouch()
    }
    
    func logInButtonDidTouch() {
        flowDelegate?.logInButtonDidTouch()
    }
}
