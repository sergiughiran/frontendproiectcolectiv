//
//  AuthenticationFlowController.swift
//  Acceptance Tests
//
//  Created by Ghiran Sergiu-Robert on 12/01/2019.
//

import UIKit

final class AuthenticationFlowController: NavigationFlowController {
    
    private var authenticationService: AuthenticationService
    private var isClient: Bool = false
    
    init(authenticationService: AuthenticationService) {
        self.authenticationService = authenticationService
        super.init(from: nil, for: .custom)
    }
    
    required init(from parent: FlowController?, for presentation: FlowControllerPresentation) {
        authenticationService = AuthenticationService()
        super.init(from: parent, for: presentation)
    }

    override func firstScreen() -> UIViewController {
        let controller = StoryboardScene.Authentication.welcomeViewController.instantiate()
        controller.viewModel = {
            let viewModel = WelcomeViewModelImpl()
            viewModel.flowDelegate = self
            return viewModel
        }()
        return controller
    }
}

extension AuthenticationFlowController: WelcomeViewModelFlowDelegate {
    func registerClientButtonDidTouch() {
        let controller = StoryboardScene.Authentication.signUpViewController.instantiate()
        isClient = true
        controller.viewModel = {
            let viewModel = SignUpViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func registerProviderButtonDidTouch() {
        let controller = StoryboardScene.Authentication.signUpViewController.instantiate()
        isClient = false
        controller.viewModel = {
            let viewModel = SignUpViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)

    }
    
    func logInButtonDidTouch() {
        let controller = StoryboardScene.Authentication.logInViewController.instantiate()
        controller.viewModel = {
            let viewModel = LogInViewModel()
            viewModel.flowDelegate = self
            return viewModel
        }()
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension AuthenticationFlowController: LogInViewModelFlowDelegate {
    func loginButtonDidTouch(email: String, password: String, success: @escaping (User) -> Void) {
        authenticationService.logIn(email: email, password: password, success: { (user) in
            UserDefaultsManager.shared.user = user
            self.authenticationService.isAuthenticated.onNext(true)
        }, failure: { error in
            self.showMessage(title: "Error", message: "The email and/or password are incorrect. Please check again")
            self.authenticationService.isAuthenticated.onNext(false)
        })
    }
}

extension AuthenticationFlowController: SignUpViewModelFlowDelegate {
    func signUp(email: String, password: String, skills: String?, displayName: String) {
        authenticationService.register(email: email, password: password, isClient: isClient, skills: skills, displayName: displayName, success: { (user) in
            UserDefaultsManager.shared.user = user
            self.authenticationService.isAuthenticated.onNext(true)
        }, failure: { error in
            self.showMessage(title: "Error", message: "We could not register you right now, please try again!")
            self.authenticationService.isAuthenticated.onNext(false)
        })
    }
}
