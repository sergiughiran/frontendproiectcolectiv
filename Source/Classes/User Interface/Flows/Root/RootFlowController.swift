//
//  RootFlowController.swift
//  project-skeleton
//
//  Created by Tamas Levente on 5/17/17.
//  Copyright © 2017 HalcyonMobile. All rights reserved.
//

import UIKit
import RxSwift

class RootFlowController {
    private weak var window: UIWindow?
    
    private var authenticationService = AuthenticationService()
    private var jobService = JobService()
    
    private var authenticationFlow: AuthenticationFlowController?
    private var dashboardFlow: DashboardFlowController?
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        authenticationService.isAuthenticated.distinctUntilChanged().subscribe(onNext: { isAuthenticated in
            if isAuthenticated {
                self.setRootToDashboard()
            } else {
                self.setRootToAuthentication()
            }
        }).disposed(by: disposeBag)
    }
    
    func setRootToDashboard() {
        dashboardFlow = DashboardFlowController(jobService: jobService, authenticationService: authenticationService)
        show(flow: dashboardFlow!)
    }
    
    private func setRootToAuthentication() {
        authenticationFlow = AuthenticationFlowController(authenticationService: authenticationService)
        show(flow: authenticationFlow!)
    }
    // MARK: - Set root
    
    fileprivate var root: UIViewController? {
        return window?.rootViewController
    }
    
    fileprivate func setRoot(to viewController: UIViewController?, animated: Bool = false) {
        guard let viewController = viewController else { return }
        
        guard root != viewController else { return }
        
        func changeRoot(to viewController: UIViewController) {
            window?.rootViewController = viewController
        }
        
        if animated, let snapshotView = window?.snapshotView(afterScreenUpdates: true) {
            viewController.view.addSubview(snapshotView)
            
            changeRoot(to: viewController)
            
            UIView.animate(withDuration: 0.33, animations: {
                snapshotView.alpha = 0.0
            }, completion: { _ in
                snapshotView.removeFromSuperview()
            })
        } else {
            changeRoot(to: viewController)
        }
    }
    
    // MARK: - Show flows
    
    fileprivate func show(flow: FlowController, animated: Bool = true) {
        flow.start { flowMainController in
            self.setRoot(to: flowMainController, animated: animated)
        }
    }
}

