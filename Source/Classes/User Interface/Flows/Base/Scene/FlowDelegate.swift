//
//  FlowDelegate.swift
//  project-skeleton
//
//  Created by Tamas Levente on 5/17/17.
//  Copyright © 2017 HalcyonMobile. All rights reserved.
//

import Foundation

protocol FlowDelegate: FlowController {
    func didGoBack()
}

extension FlowDelegate {
    
    func didGoBack() {
        popNavigationStack()
    }
}
