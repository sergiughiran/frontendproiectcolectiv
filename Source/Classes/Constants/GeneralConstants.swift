//
//  GeneralConstants.swift
//  project-skeleton
//
//  Created by Botond Magyarosi on 12/07/16.
//  Copyright © 2016 Halcyon Mobile. All rights reserved.
//

import UIKit

let SharedApplication = UIApplication.shared
let NotificationCenter = Foundation.NotificationCenter.default
