//
//  Types.swift
//  project-skeleton
//
//  Created by Tamas Levente on 10/14/16.
//  Copyright © 2016 Halcyon Mobile. All rights reserved.
//

import Foundation

// TODO: See if this matches with your id type
typealias ID = Int
typealias Percent = Double  // between 0..1

let IDNotSet: ID = 0

