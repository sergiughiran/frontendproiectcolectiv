// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum Authentication: StoryboardType {
    internal static let storyboardName = "Authentication"

    internal static let logInViewController = SceneType<iOSFrontend.LogInViewController>(storyboard: Authentication.self, identifier: "LogInViewController")

    internal static let signUpViewController = SceneType<iOSFrontend.SignUpViewController>(storyboard: Authentication.self, identifier: "SignUpViewController")

    internal static let welcomeViewController = SceneType<iOSFrontend.WelcomeViewController>(storyboard: Authentication.self, identifier: "WelcomeViewController")
  }
  internal enum Dashboard: StoryboardType {
    internal static let storyboardName = "Dashboard"

    internal static let addJobViewController = SceneType<iOSFrontend.AddJobViewController>(storyboard: Dashboard.self, identifier: "AddJobViewController")

    internal static let changePasswordViewController = SceneType<iOSFrontend.ChangePasswordViewController>(storyboard: Dashboard.self, identifier: "ChangePasswordViewController")

    internal static let clientJobViewController = SceneType<iOSFrontend.ClientJobViewController>(storyboard: Dashboard.self, identifier: "ClientJobViewController")

    internal static let jobListViewController = SceneType<iOSFrontend.JobListViewController>(storyboard: Dashboard.self, identifier: "JobListViewController")

    internal static let ongoingJobDetailViewController = SceneType<iOSFrontend.OngoingJobDetailViewController>(storyboard: Dashboard.self, identifier: "OngoingJobDetailViewController")

    internal static let ongoingJobsViewController = SceneType<iOSFrontend.OngoingJobsViewController>(storyboard: Dashboard.self, identifier: "OngoingJobsViewController")

    internal static let providerInfoViewController = SceneType<iOSFrontend.ProviderInfoViewController>(storyboard: Dashboard.self, identifier: "ProviderInfoViewController")

    internal static let providerJobViewController = SceneType<iOSFrontend.ProviderJobViewController>(storyboard: Dashboard.self, identifier: "ProviderJobViewController")

    internal static let providerRequestViewController = SceneType<iOSFrontend.ProviderRequestViewController>(storyboard: Dashboard.self, identifier: "ProviderRequestViewController")

    internal static let userDetailViewController = SceneType<iOSFrontend.UserDetailViewController>(storyboard: Dashboard.self, identifier: "UserDetailViewController")

    internal static let userListViewController = SceneType<iOSFrontend.UserListViewController>(storyboard: Dashboard.self, identifier: "UserListViewController")

    internal static let userProfileViewController = SceneType<iOSFrontend.UserProfileViewController>(storyboard: Dashboard.self, identifier: "UserProfileViewController")

    internal static let userRequestsViewController = SceneType<iOSFrontend.UserRequestsViewController>(storyboard: Dashboard.self, identifier: "UserRequestsViewController")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "Launch Screen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: Bundle(for: BundleToken.self))
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

private final class BundleToken {}
